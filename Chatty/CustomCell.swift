//
//  CustomCell.swift
//  Chatty
//
//  Created by Guneet on 2019-02-20.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var receiverText: UITextView!
  
    @IBOutlet weak var senderText: UITextView!
    
}
