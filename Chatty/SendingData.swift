//
//  SendingData.swift
//  Chatty
//
//  Created by Guneet on 2019-02-22.
//  Copyright © 2019 Guneet. All rights reserved.
//

import Foundation

class SendingData {
    var sendMessage:String
    init(sendMessage:String) {
        self.sendMessage = sendMessage
    }
}
