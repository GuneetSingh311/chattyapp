//
//  ViewController.swift
//  Chatty
//
//  Created by Guneet on 2019-02-20.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import Assistant
class HomeController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   

    
    @IBOutlet weak var messageInput: UITextField!
    @IBOutlet weak var chattyTableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    
    let assistant = Assistant(version: "2019-02-20", apiKey: "5hl-h3nUyEZjmTPdVSO4ATsiHoPvMqm5iuXpjb0nsFy1")
    let workspace = "7f879f29-5834-476e-b735-9711e5b327cf"
    // save context to state to continue the conversation later
    var context: Context?
    var messages:[String]!
    var datafromServer = [DataFromServer]()
    var sendingData = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Start a conversation
       
        self.updateTable()
        chattyTableView.delegate = self
        chattyTableView.dataSource=self
        assistant.message(workspaceID: "7f879f29-5834-476e-b735-9711e5b327cf") { response, error in
        if let error = error {
            print(error)
        }
        
        guard let message = response?.result else {
            print("Failed to get the message.")
            return
        }
        
        print("Conversation ID: \(message.context.conversationID!)")
        print("Response: \(message.output.text.joined())")
    
        // Set the context to state
        self.context = message.context
    }
       
        
        
       
       }
    func updateTable() {
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
            self.chattyTableView.reloadData()
        }
        
    }
    
    
        @IBAction func sendAction(_ sender: Any) {
        let input = InputData(text: self.messageInput.text!)
       self.sendingData.append(input.text)
        self.updateTable()
            print(self.sendingData)
            assistant.message(workspaceID: workspace, input: input, context: self.context) { response, error in
                if let error = error {
                    print(error)
                }
                
                guard let message = response?.result else {
                    print("Failed to get the message.")
                    return
                }
                
                print("Conversation ID: \(message.context.conversationID!)")
                print("Response: \(message.output.text.joined())")
                self.datafromServer.append(DataFromServer(message: message.output.text.joined()))
               
                
                print(self.datafromServer.count)
                
                // Update the context
                self.context = message.context
            }
        
    
    
    }
    
    
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datafromServer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "chatCellReceiver", for: indexPath) as! CustomCell
     
        if(cell.receiverText.text == "") {
            cell.receiverText.text = "typing..."
        }
        else {
        cell.receiverText.text = self.datafromServer[indexPath.row].message
        cell.receiverText.layer.cornerRadius = 2.0
        cell.receiverText.isEditable = false
        cell.receiverText.isSelectable = true
        }
        
       if (cell.senderText.text == "") {
        cell.senderText.text = "..."
        }
        else
        { self.updateTable()
        cell.senderText.text = self.sendingData[indexPath.row]
        cell.senderText.layer.cornerRadius = 2.0
        cell.senderText.isEditable = false
        cell.senderText.isSelectable = true
        }
    
        return cell
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    

}

